const http = require('http');
const { v4: uuidv4 } = require('uuid');


const PORT = process.env.PORT || 4000;


const server = http.createServer((req, res) => {

    if (req.method === 'GET' && req.url === '/html') {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        const htmlContent = `<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
          </body>
        </html>`;
        res.end(htmlContent)
    }

    if (req.method === 'GET' && req.url === '/') {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end("HTTP DRILL")
    }

    if (req.method === 'GET' && req.url === '/json') {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        const jsonData = `
        {
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }
        `
        res.end(jsonData)
    }

    if (req.method === 'GET' && req.url === '/uuid') {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        const data = {
            uuid: uuidv4()
        }
        res.end(data)
    }

    if (req.method === 'GET' && req.url.startsWith('/status/')) {
        const statusCode = parseInt(req.url.split('/')[2]);

        if (isNaN(statusCode)) {
            res.writeHead(400, { 'Content-Type': 'text/plain' });
            res.end('Status code is not correct');
        } else {
            res.writeHead(statusCode, { 'Content-Type': 'text/plain' });
            res.end(`${statusCode} ${http.STATUS_CODES[statusCode]}`)

        }
    }

    if (req.method === 'GET' && req.url.startsWith('/delay/')) {
        const delayInSeconds = parseInt(req.url.split('/')[2]);
        if (isNaN(delayInSeconds)) {
            res.writeHead(400, { 'Content-Type': 'text/plain' });
            res.end('Delay value is invalid');
        } else {
            setTimeout(() => {
                res.writeHead(200, { 'Content-Type': 'text/plain' });
                res.end(`200 : Response success and delayed by ${delayInSeconds} seconds`);
            }, delayInSeconds * 1000);
        }
    }


})


server.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
});
